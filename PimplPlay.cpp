#include "SillyThing.hpp"

#include <iostream>

int main()
{
	SillyThing thing;
	thing.AddThing(2);
	thing.AddThing(4);
	thing.AddThing(6);
	thing.AddThing(8);

	std::cout << thing.GetThing(2) << std::endl;
	std::cout << thing.GetRandom() << std::endl;
	std::cout << thing.GetFreakyRandom() << std::endl;

	return 0;
}
