#ifndef SILLY_THING_HPP
#define SILLY_THING_HPP

#include <memory>

class SillyThing
{
	struct SillyThingImpl;
	std::unique_ptr<SillyThingImpl> pimpl;
public:
	SillyThing();
	~SillyThing();

	void AddThing(int const);
	int GetThing(int const) const;
	int GetRandom() const;
	int GetFreakyRandom() const;
};

#endif