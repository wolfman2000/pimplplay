#include "SillyThing.hpp"

#include <vector>
#include <random>
#include <functional>

struct SillyThing::SillyThingImpl
{
	std::vector<int> things;
	SillyThing & orig;

	SillyThingImpl(SillyThing *);

	int GetRandomIndex() const;
	int GetRandomFromPublic() const;

	std::uniform_int_distribution<unsigned int> unif;
	std::function<unsigned int()> rnd;
};

SillyThing::SillyThingImpl::SillyThingImpl(SillyThing * that) : orig(*that)
{
}

SillyThing::SillyThing() : pimpl(new SillyThingImpl(this))
{
	std::uniform_int_distribution<unsigned int> unif;
	std::random_device rd;
	std::mt19937 engine(rd());
	pimpl->rnd = std::bind(unif, engine);
}

SillyThing::~SillyThing()
{
}

void SillyThing::AddThing(int const num)
{
	pimpl->things.push_back(num);
}

int SillyThing::GetThing(int const index) const
{
	return pimpl->things[index];
}

int SillyThing::GetRandom() const
{
	int index = pimpl->GetRandomIndex();
	return GetThing(index);
}

int SillyThing::SillyThingImpl::GetRandomIndex() const
{
	// I know this is not the greatest implementation.
	return rnd() % things.size();
}

int SillyThing::GetFreakyRandom() const
{
	return pimpl->GetRandomFromPublic();
}

int SillyThing::SillyThingImpl::GetRandomFromPublic() const
{
	return orig.GetRandom();
}